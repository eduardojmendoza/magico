-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: boot_jersey
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `SPRING_SESSION_ATTRIBUTES`
--

DROP TABLE IF EXISTS `SPRING_SESSION_ATTRIBUTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SPRING_SESSION_ATTRIBUTES` (
  `SESSION_PRIMARY_ID` char(36) NOT NULL,
  `ATTRIBUTE_NAME` varchar(200) NOT NULL,
  `ATTRIBUTE_BYTES` blob NOT NULL,
  PRIMARY KEY (`SESSION_PRIMARY_ID`,`ATTRIBUTE_NAME`),
  KEY `SPRING_SESSION_ATTRIBUTES_IX1` (`SESSION_PRIMARY_ID`),
  CONSTRAINT `SPRING_SESSION_ATTRIBUTES_FK` FOREIGN KEY (`SESSION_PRIMARY_ID`) REFERENCES `SPRING_SESSION` (`PRIMARY_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SPRING_SESSION_ATTRIBUTES`
--

LOCK TABLES `SPRING_SESSION_ATTRIBUTES` WRITE;
/*!40000 ALTER TABLE `SPRING_SESSION_ATTRIBUTES` DISABLE KEYS */;
INSERT INTO `SPRING_SESSION_ATTRIBUTES` VALUES ('ff0bf7f4-bfa4-48eb-a223-41722d007289','org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository.CSRF_TOKEN',_binary '�\�\0sr\06org.springframework.security.web.csrf.DefaultCsrfTokenZ\�\�/��\�\0L\0\nheaderNamet\0Ljava/lang/String;L\0\rparameterNameq\0~\0L\0tokenq\0~\0xpt\0X-CSRF-TOKENt\0_csrft\0$2b401dfd-053a-4b68-94ef-6f0fc943e5bb'),('ff0bf7f4-bfa4-48eb-a223-41722d007289','SPRING_SECURITY_LAST_EXCEPTION',_binary '�\�\0sr\0Corg.springframework.security.authentication.BadCredentialsException&N�d�]\0\0xr\09org.springframework.security.core.AuthenticationExceptionQ0\�gT\0\0xr\0\Zjava.lang.RuntimeException�_G\n4�\�\0\0xr\0java.lang.Exception\��>\Z;\�\0\0xr\0java.lang.Throwable\�\�5\'9w�\�\0L\0causet\0Ljava/lang/Throwable;L\0\rdetailMessaget\0Ljava/lang/String;[\0\nstackTracet\0[Ljava/lang/StackTraceElement;L\0suppressedExceptionst\0Ljava/util/List;xpq\0~\0	t\0Bad credentialsur\0[Ljava.lang.StackTraceElement;F*<<�\"9\0\0xp\0\0\0=sr\0java.lang.StackTraceElementa	Ś&6݅\0I\0\nlineNumberL\0declaringClassq\0~\0L\0fileNameq\0~\0L\0\nmethodNameq\0~\0xp\0\0\0�t\0Yorg.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvidert\0.AbstractUserDetailsAuthenticationProvider.javat\0authenticatesq\0~\0\r\0\0\0�t\0;org.springframework.security.authentication.ProviderManagert\0ProviderManager.javaq\0~\0sq\0~\0\r\0\0\0\�q\0~\0q\0~\0q\0~\0sq\0~\0\r\0\0\0^t\0Torg.springframework.security.web.authentication.UsernamePasswordAuthenticationFiltert\0)UsernamePasswordAuthenticationFilter.javat\0attemptAuthenticationsq\0~\0\r\0\0\0\�t\0Vorg.springframework.security.web.authentication.AbstractAuthenticationProcessingFiltert\0+AbstractAuthenticationProcessingFilter.javat\0doFiltersq\0~\0\r\0\0Nt\0Dorg.springframework.security.web.FilterChainProxy$VirtualFilterChaint\0FilterChainProxy.javaq\0~\0sq\0~\0\r\0\0\0tt\0Corg.springframework.security.web.authentication.logout.LogoutFiltert\0LogoutFilter.javaq\0~\0sq\0~\0\r\0\0Nq\0~\0q\0~\0 q\0~\0sq\0~\0\r\0\0\0|t\00org.springframework.security.web.csrf.CsrfFiltert\0CsrfFilter.javat\0doFilterInternalsq\0~\0\r\0\0\0kt\03org.springframework.web.filter.OncePerRequestFiltert\0OncePerRequestFilter.javaq\0~\0sq\0~\0\r\0\0Nq\0~\0q\0~\0 q\0~\0sq\0~\0\r\0\0\0Bt\0:org.springframework.security.web.header.HeaderWriterFiltert\0HeaderWriterFilter.javaq\0~\0(sq\0~\0\r\0\0\0kq\0~\0*q\0~\0+q\0~\0sq\0~\0\r\0\0Nq\0~\0q\0~\0 q\0~\0sq\0~\0\r\0\0\0it\0Iorg.springframework.security.web.context.SecurityContextPersistenceFiltert\0%SecurityContextPersistenceFilter.javaq\0~\0sq\0~\0\r\0\0Nq\0~\0q\0~\0 q\0~\0sq\0~\0\r\0\0\08t\0Worg.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFiltert\0%WebAsyncManagerIntegrationFilter.javaq\0~\0(sq\0~\0\r\0\0\0kq\0~\0*q\0~\0+q\0~\0sq\0~\0\r\0\0Nq\0~\0q\0~\0 q\0~\0sq\0~\0\r\0\0\0\�t\01org.springframework.security.web.FilterChainProxyq\0~\0 q\0~\0(sq\0~\0\r\0\0\0�q\0~\0<q\0~\0 q\0~\0sq\0~\0\r\0\0et\04org.springframework.web.filter.DelegatingFilterProxyt\0\ZDelegatingFilterProxy.javat\0invokeDelegatesq\0~\0\r\0\0q\0~\0?q\0~\0@q\0~\0sq\0~\0\r\0\0\0�t\0/org.apache.catalina.core.ApplicationFilterChaint\0ApplicationFilterChain.javat\0internalDoFiltersq\0~\0\r\0\0\0�q\0~\0Dq\0~\0Eq\0~\0sq\0~\0\r\0\0\0ct\03org.springframework.web.filter.RequestContextFiltert\0RequestContextFilter.javaq\0~\0(sq\0~\0\r\0\0\0kq\0~\0*q\0~\0+q\0~\0sq\0~\0\r\0\0\0�q\0~\0Dq\0~\0Eq\0~\0Fsq\0~\0\r\0\0\0�q\0~\0Dq\0~\0Eq\0~\0sq\0~\0\r\0\0\0mt\07org.springframework.web.filter.HttpPutFormContentFiltert\0HttpPutFormContentFilter.javaq\0~\0(sq\0~\0\r\0\0\0kq\0~\0*q\0~\0+q\0~\0sq\0~\0\r\0\0\0�q\0~\0Dq\0~\0Eq\0~\0Fsq\0~\0\r\0\0\0�q\0~\0Dq\0~\0Eq\0~\0sq\0~\0\r\0\0\0]t\05org.springframework.web.filter.HiddenHttpMethodFiltert\0HiddenHttpMethodFilter.javaq\0~\0(sq\0~\0\r\0\0\0kq\0~\0*q\0~\0+q\0~\0sq\0~\0\r\0\0\0�q\0~\0Dq\0~\0Eq\0~\0Fsq\0~\0\r\0\0\0�q\0~\0Dq\0~\0Eq\0~\0sq\0~\0\r\0\0\0�t\0<org.springframework.session.web.http.SessionRepositoryFiltert\0SessionRepositoryFilter.javaq\0~\0(sq\0~\0\r\0\0\0Qt\09org.springframework.session.web.http.OncePerRequestFilterq\0~\0+q\0~\0sq\0~\0\r\0\0\0�q\0~\0Dq\0~\0Eq\0~\0Fsq\0~\0\r\0\0\0�q\0~\0Dq\0~\0Eq\0~\0sq\0~\0\r\0\0\0\�t\06org.springframework.web.filter.CharacterEncodingFiltert\0CharacterEncodingFilter.javaq\0~\0(sq\0~\0\r\0\0\0kq\0~\0*q\0~\0+q\0~\0sq\0~\0\r\0\0\0�q\0~\0Dq\0~\0Eq\0~\0Fsq\0~\0\r\0\0\0�q\0~\0Dq\0~\0Eq\0~\0sq\0~\0\r\0\0\0\�t\0-org.apache.catalina.core.StandardWrapperValvet\0StandardWrapperValve.javat\0invokesq\0~\0\r\0\0\0`t\0-org.apache.catalina.core.StandardContextValvet\0StandardContextValve.javaq\0~\0jsq\0~\0\r\0\0\�t\03org.apache.catalina.authenticator.AuthenticatorBaset\0AuthenticatorBase.javaq\0~\0jsq\0~\0\r\0\0\0�t\0*org.apache.catalina.core.StandardHostValvet\0StandardHostValve.javaq\0~\0jsq\0~\0\r\0\0\0Qt\0+org.apache.catalina.valves.ErrorReportValvet\0ErrorReportValve.javaq\0~\0jsq\0~\0\r\0\0\0Wt\0,org.apache.catalina.core.StandardEngineValvet\0StandardEngineValve.javaq\0~\0jsq\0~\0\r\0\0Vt\0+org.apache.catalina.connector.CoyoteAdaptert\0CoyoteAdapter.javat\0servicesq\0~\0\r\0\0 t\0(org.apache.coyote.http11.Http11Processort\0Http11Processor.javaq\0~\0}sq\0~\0\r\0\0\0Bt\0(org.apache.coyote.AbstractProcessorLightt\0AbstractProcessorLight.javat\0processsq\0~\0\r\0\0 t\04org.apache.coyote.AbstractProtocol$ConnectionHandlert\0AbstractProtocol.javaq\0~\0�sq\0~\0\r\0\0�t\06org.apache.tomcat.util.net.NioEndpoint$SocketProcessort\0NioEndpoint.javat\0doRunsq\0~\0\r\0\0\01t\0.org.apache.tomcat.util.net.SocketProcessorBaset\0SocketProcessorBase.javat\0runsq\0~\0\r\0\0}t\0\'java.util.concurrent.ThreadPoolExecutort\0ThreadPoolExecutor.javat\0	runWorkersq\0~\0\r\0\0pt\0.java.util.concurrent.ThreadPoolExecutor$Workerq\0~\0�q\0~\0�sq\0~\0\r\0\0\0=t\0:org.apache.tomcat.util.threads.TaskThread$WrappingRunnablet\0TaskThread.javaq\0~\0�sq\0~\0\r\0\0\�t\0java.lang.Threadt\0Thread.javaq\0~\0�sr\0&java.util.Collections$UnmodifiableList�%1�\�\0L\0listq\0~\0xr\0,java.util.Collections$UnmodifiableCollectionB\0�\�^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.ArrayListx�\��\�a�\0I\0sizexp\0\0\0\0w\0\0\0\0xq\0~\0�x'),('ff0bf7f4-bfa4-48eb-a223-41722d007289','SPRING_SECURITY_SAVED_REQUEST',_binary '�\�\0sr\0Aorg.springframework.security.web.savedrequest.DefaultSavedRequest@HD�6d�\0I\0\nserverPortL\0contextPatht\0Ljava/lang/String;L\0cookiest\0Ljava/util/ArrayList;L\0headerst\0Ljava/util/Map;L\0localesq\0~\0L\0methodq\0~\0L\0\nparametersq\0~\0L\0pathInfoq\0~\0L\0queryStringq\0~\0L\0\nrequestURIq\0~\0L\0\nrequestURLq\0~\0L\0schemeq\0~\0L\0\nserverNameq\0~\0L\0servletPathq\0~\0xp\0\0�t\0\0sr\0java.util.ArrayListx�\��\�a�\0I\0sizexp\0\0\0w\0\0\0sr\09org.springframework.security.web.savedrequest.SavedCookie@+����f\0I\0maxAgeZ\0secureI\0versionL\0commentq\0~\0L\0domainq\0~\0L\0nameq\0~\0L\0pathq\0~\0L\0valueq\0~\0xp����\0\0\0\0\0ppt\0\nJSESSIONIDpt\0 9348DDB42885CE521826EB660A37F97Fsq\0~\0����\0\0\0\0\0ppt\0SESSIONpt\00M2YzMDQ2Y2ItMGNhZC00YTYzLTk2NmUtZjhkNjU5YjQ2MjQ0xsr\0java.util.TreeMap��>-%j\�\0L\0\ncomparatort\0Ljava/util/Comparator;xpsr\0*java.lang.String$CaseInsensitiveComparatorw\\}\\P\�\�\0\0xpw\0\0\0t\0acceptsq\0~\0\0\0\0w\0\0\0t\0?text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8xt\0accept-encodingsq\0~\0\0\0\0w\0\0\0t\0\rgzip, deflatext\0accept-languagesq\0~\0\0\0\0w\0\0\0t\0en-GB,en;q=0.5xt\0\nconnectionsq\0~\0\0\0\0w\0\0\0t\0\nkeep-alivext\0cookiesq\0~\0\0\0\0w\0\0\0t\0+JSESSIONID=9348DDB42885CE521826EB660A37F97Fxt\0hostsq\0~\0\0\0\0w\0\0\0t\0localhost:8080xt\0upgrade-insecure-requestssq\0~\0\0\0\0w\0\0\0t\01xt\0\nuser-agentsq\0~\0\0\0\0w\0\0\0t\0LMozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0xxsq\0~\0\0\0\0w\0\0\0sr\0java.util.Locale~�`�0�\�\0I\0hashcodeL\0countryq\0~\0L\0\nextensionsq\0~\0L\0languageq\0~\0L\0scriptq\0~\0L\0variantq\0~\0xp����t\0GBq\0~\0t\0enq\0~\0q\0~\0xsq\0~\0-����q\0~\0q\0~\0q\0~\00q\0~\0q\0~\0xxt\0GETsq\0~\0pw\0\0\0\0xt\0/userspt\0/userst\0http://localhost:8080/userst\0httpt\0	localhostt\0\0');
/*!40000 ALTER TABLE `SPRING_SESSION_ATTRIBUTES` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-04 17:24:09
