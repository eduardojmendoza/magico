package com.test.boot.config;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
 

@EnableTransactionManagement
@EnableJpaRepositories("com.test.boot.repositories")
public class SpringDataConfig {
 
}
