package com.test.boot.repositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import com.test.boot.entities.User;

@Component
public interface UserRepository extends CrudRepository<User, Integer> {
}
